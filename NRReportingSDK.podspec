Pod::Spec.new do |s|  
    s.name              = 'NRReportingSDK'
    s.version           = '1.1.0'
    s.summary           = 'NextRadio Reporting SDK automates much of the TagStation reporting process with a few lines of code.'


    s.description       = <<-DESC
     The NextRadio Reporting SDK automates much of the TagStation reporting process by managing device registration, location retrieval, and listening session reports.
      The SDK is lightweight and requires just a few lines of code to integrate with your existing application.
                           DESC

    s.homepage          = 'http://nextradioapp.com'
    s.author            = { 'Cody Nelson' => 'cnelson@bonneville.com' }
    s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

    s.platform          = :ios
    s.source            = { :git => 'https://code4121@bitbucket.org/bonneville/nextradiosdkpodfork.git', :tag => s.version.to_s }
    s.source_files      = 'NRReportingSDK/Classes/**/*'
    s.ios.deployment_target = '10.0'
    #s.ios.vendored_frameworks = 'NRReportingSDK-1.1.0/NRReportingSDK.framework'
   # s.frameworks = 'Foundation'
end  
