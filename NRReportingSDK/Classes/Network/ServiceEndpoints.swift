//
//  Created by John Koszarek on 12/18/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation

///  Service endpoints used with TagStation, such as the DataIntake REST API.
final class ServiceEndpoints {

    /**
     DataIntake REST API endpoints.
     */
    struct DataIntake {
        static let endpoint = "https://api.tagstation.com/sdk/v1.0/usage"
        static let endpointDev = "https://dev-api.tagstation.com/sdk/v1.0/usage"

        // Retruns the service endpoint to use based on whether we're running in a production or
        // development environment.
        static func serviceEndpoint(isDevelopmentEnvironment: Bool) -> String {
            if isDevelopmentEnvironment {
                return endpointDev
            }
            else {
                return endpoint
            }
        }
    }
}
