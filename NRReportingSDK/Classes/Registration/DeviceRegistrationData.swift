//  Created by John Koszarek on 10/25/17.
//  Copyright © 2017 NextRadio. All rights reserved.

import Foundation
import UIKit
import CoreTelephony
import AdSupport

/// Data used when registering a device.
/// DeviceRegistrationData objects are serializable.
public final class DeviceRegistrationData: NSObject, NSCoding, Archivable {

    static let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let archiveUrl = documentsDirectory.appendingPathComponent("tsaDeviceRegistrationData")

    public enum DeviceRegistrationDataError: Error {
        case unableToArchive
        case unableToDelete
    }

    // Keys used with the dictionary of device registration data key/value pairs.
    // Used for required properties.
    static let brandKey = "brand"                   // brand name
    static let clientNameKey = "clientName"         // client name
    static let deviceKey = "device"                 // device product name
    static let manufacturerKey = "manufacturer"     // device manufacturer
    static let modelKey = "model"                   // device model

    // Keys used with the dictionary of device registration data key/value pairs.
    // Used for optional properties.
    static let adIdKey = "adId"                     // advertising ID
    static let appVersionKey = "appVersion"         // app version (formerly nextradio_version)
    static let carrierKey = "carrier"               // carrier
    static let countryKey = "country"               // ISO country code
    static let localeKey = "locale"                 // locale
    static let macAddressKey = "macAddress"         // MAC address
    static let otherInformationKey = "otherInfo"    // other information
    static let sdkVersionKey = "sdkVersion"         // TagStation Analytics SDK version
    static let systemVersionKey = "systemVersion"   // system software version
    static let systemNameKey = "systemSoftware"     // name of the OS running on the device

    static let defaultBrandName = "APPLE"
    static let defaultDeviceManufacturer = "APPLE"

    // Dictionary of device registration data key/value pairs.
    var values = [String : String]()

    /**
     Creates a new DeviceRegistrationData object and sets the following properties:
     Brand name, client name, device product name, device manufacturer, device model, advertising ID,
     app version, carrier, ISO country code, locale, and MAC address.

     Parameter nextRadioReportingKey: The name of a host media app as it identifies itself to TagStation.
     Parameter shouldAutoInitProperties: True if this class should set as many of its properties as
     possible, false if not.
     */
    public init(nextRadioReportingKey: String, shouldAutoInitProperties: Bool = true) {
        super.init()

        if shouldAutoInitProperties != true {
            return
        }

        // Required properties.
        brandName = DeviceRegistrationData.defaultBrandName
        clientName = nextRadioReportingKey
        deviceProductName = UIDevice.current.model                              // TagStation calls this "device"
        deviceManufacturer = DeviceRegistrationData.defaultDeviceManufacturer   // TagStation calls this "manufacturer"
        deviceModel = UIDevice.current.modelName                                // TagStation calls this "model"

        // Optional properties.
        advertisingId = ASIdentifierManager.shared().advertisingIdentifier.uuidString
        analyticsSdkVersion = FrameworkConstants.sdkVersion                     // TagStation calls this "sdkVersion"
        appVersion = (Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String)
        locale = Locale.current.identifier
        macAddress = UIDevice.current.identifierForVendor?.uuidString
        systemSoftwareName = UIDevice.current.systemName                        // TagStation calls this "systemSoftware"
        systemSoftwareVersion = UIDevice.current.systemVersion                  // TagStation calls this "systemVersion"

        let telephoneNetworkInfo = CTTelephonyNetworkInfo()
        carrier = telephoneNetworkInfo.subscriberCellularProvider?.carrierName

        let alpha2isoCountryCode = Locale.current.regionCode
        isoCountryCode = IsoCountryCodes.find(key: alpha2isoCountryCode!).alpha3    // TagStation calls this "country"
    }

    /**
     Creates a new DeviceRegistrationData object and sets properties using the values in the supplied dictionary.
     This method is intended for use when unarchiving previously saved DeviceRegistrationData.
     */
    public init(values: [AnyHashable: Any]) {
        self.values = NSDictionary(dictionary: values) as! [String : String]
    }

    /**
     Brand name of the device. Required by TagStation Analytics.
     */
    public var brandName: String? {
        get {
            return values[DeviceRegistrationData.brandKey]
        }
        set {
            setValue(newValue, forKey: DeviceRegistrationData.brandKey, maxLength: 100)
        }
    }

    /**
     * Identifies a client, such as the platform/OS. Required by TagStation Analytics.
     */
    public var clientName: String? {
        get {
            return values[DeviceRegistrationData.clientNameKey]!
        }
        set {
            setValue(newValue, forKey: DeviceRegistrationData.clientNameKey, maxLength: 100)
        }
    }

    /**
     Device product name. Required by TagStation Analytics.
     */
    public var deviceProductName: String? {
        get {
            return values[DeviceRegistrationData.deviceKey]!
        }
        set {
            setValue(newValue, forKey: DeviceRegistrationData.deviceKey, maxLength: 100)
        }
    }

    /**
     Device manufacturer. Required by TagStation Analytics.
     */
    public var deviceManufacturer: String? {
        get {
            return values[DeviceRegistrationData.manufacturerKey]!
        }
        set {
            setValue(newValue, forKey: DeviceRegistrationData.manufacturerKey, maxLength: 100)
        }
    }

    /**
     Device model. Required by TagStation Analytics.
     */
    public var deviceModel: String? {
        get {
            return values[DeviceRegistrationData.modelKey]
        }
        set {
            setValue(newValue, forKey: DeviceRegistrationData.modelKey, maxLength: 100)
        }
    }

    /**
     Advertising ID (e.g., Goodle Ad ID).
     */
    public var advertisingId: String? {
        get {
            return values[DeviceRegistrationData.adIdKey]
        }
        set {
            setValue(newValue, forKey: DeviceRegistrationData.adIdKey, maxLength: 100)
        }
    }

    /**
     TagStation Analytics SDK version.
     */
    public var analyticsSdkVersion : String? {
        get {
            return values[DeviceRegistrationData.sdkVersionKey]
        }
        set {
            setValue(newValue, forKey: DeviceRegistrationData.sdkVersionKey, maxLength: 100)
        }
    }

    /**
     App version or NextRadio/TagStation supplied registration key.
     */
    public var appVersion: String? {
        get {
            return values[DeviceRegistrationData.appVersionKey]
        }
        set {
            setValue(newValue, forKey: DeviceRegistrationData.appVersionKey, maxLength: 100)
        }
    }

    /**
     The carrier network that the device is connected to.
     */
    public var carrier: String? {
        get {
            return values[DeviceRegistrationData.carrierKey]
        }
        set {
            setValue(newValue, forKey: DeviceRegistrationData.carrierKey, maxLength: 100)
        }
    }

    /**
     The three character ISO country code where the device is being used (e.g., USA).
     */
    public var isoCountryCode: String? {
        get {
            return values[DeviceRegistrationData.countryKey]
        }
        set {
            // Note other than checking the length of the supplied string value, we rely upon the
            // TagStation database for country code validation.
            setValue(newValue, forKey: DeviceRegistrationData.countryKey, maxLength: 3)
        }
    }

    /**
     Locale (e.g., en_US).
     */
    public var locale: String? {
        get {
            return values[DeviceRegistrationData.localeKey]
        }
        set {
            // Rely upon the TagStation database for validation.
            setValue(newValue, forKey: DeviceRegistrationData.localeKey, maxLength: Int.max)
        }
    }

    /**
     Device MAC address. For iOS, this will be the "identifier for vendor" as a UUID.
     */
    public var macAddress: String? {
        get {
            return values[DeviceRegistrationData.macAddressKey]
        }
        set {
            setValue(newValue, forKey: DeviceRegistrationData.macAddressKey, maxLength: Int.max)
        }
    }

    /**
     Information about a device that doesn't fall under any other category.
     */
    public var otherInformation: String? {
        get {
            return values[DeviceRegistrationData.otherInformationKey]
        }
        set {
            setValue(newValue, forKey: DeviceRegistrationData.otherInformationKey, maxLength: 300)
        }
    }

    /**
     System software name.
     */
    public var systemSoftwareName: String? {
        get {
            return values[DeviceRegistrationData.systemNameKey]
        }
        set {
            setValue(newValue, forKey: DeviceRegistrationData.systemNameKey, maxLength: 50)
        }
    }

    /**
     System software version.
     */
    public var systemSoftwareVersion: String? {
        get {
            return values[DeviceRegistrationData.systemVersionKey]
        }
        set {
            setValue(newValue, forKey: DeviceRegistrationData.systemVersionKey, maxLength: 100)
        }
    }

    /**
     Returns a Boolean value that indicating if two DeviceRegistrationData objects are equal.
     */
    public func isEqual(to other: DeviceRegistrationData?) -> Bool {
        guard other != nil else {
            return false
        }
        let nsDictionary = NSDictionary(dictionary: values)
        return nsDictionary.isEqual(to: other!.values)
    }

    /**
     Converts this object to JSON which is then returned as Data (bytes).
     */
    public func jsonAsData(enclosingObjectName: String? = nil) -> Data? {
        if enclosingObjectName != nil {
            let enclosingObject = [
                enclosingObjectName! : values
            ]
            guard let newData = try? JSONEncoder().encode(enclosingObject) else {
                return nil
            }
            return newData
        }
        else {
            guard let newData = try? JSONEncoder().encode(values) else {
                return nil
            }
            return newData
        }
    }

    private func setValue(_ value: String?, forKey key: String, maxLength: Int) {
        if value != nil && !value!.isEmpty {
            let newValue = String.init(value!.prefix(maxLength))
            values[key] = newValue
        }
        else {
            values.removeValue(forKey: key)
        }
    }

    // MARK: NSCoding protocol

    /**
     Returns an object initialized from data in a given unarchiver.
     */
    public required convenience init?(coder aDecoder: NSCoder) {
        guard let decodedValues = aDecoder.decodeObject(forKey: "valuesKey") as? [AnyHashable : Any] else {
            return nil
        }
        self.init(values: decodedValues)
    }

    /**
     Encodes the receiver using a given archiver.
     */
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(values, forKey: "valuesKey")
    }

    // MARK: Archivable protocol

    public func saveObject() throws {
        let success = NSKeyedArchiver.archiveRootObject(self, toFile: DeviceRegistrationData.archiveUrl.path)
        if !success {
            throw DeviceRegistrationDataError.unableToArchive
        }
    }

    public class func loadObject() -> Any? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: DeviceRegistrationData.archiveUrl.path)
    }

    public class func deleteObject() throws {
        let exists = FileManager().fileExists(atPath: DeviceRegistrationData.archiveUrl.path)
        if exists {
            do {
                try FileManager().removeItem(atPath: DeviceRegistrationData.archiveUrl.path)
            }
            catch {
                throw DeviceRegistrationDataError.unableToDelete
            }
        }
    }
}
