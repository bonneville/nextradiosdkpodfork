//
//  Created by John Koszarek on 12/4/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation
import AVFoundation
import CoreLocation
import os.log

/// Business logic for determining if the user's listening session is active or not.
final class ListeningSession: NSObject {

    static let sharedInstance = ListeningSession()

    enum ListeningSessionSource: Int {
        case noSession = 0
        case listeningSession = 1
        case radioEvent = 2
    }

    private(set) var sessionIsActive = false

    private(set) var sessionSource = ListeningSessionSource.noSession

    private var sessionListeningChannel : SessionListeningChannel?
    var hasSessionListeningChannel : Bool {
        return sessionListeningChannel != nil ? true : false
    }

    private var impressionRadioEvent: ImpressionRadioEvent?
    var hasImpressionRadioEvent: Bool {
        return impressionRadioEvent != nil ? true : false
    }

    private(set) var volumeIsMuted: Bool

    private struct VolumeObservation {
        static let volumeKey = "outputVolume"
        static var context = 0
    }

    /** Called to inform an observer of when a user's listening session has started or ended. */
    var delegate: ListeningSessionDelegate?

    private override init() {
        let volume: Float = AVAudioSession.sharedInstance().outputVolume
        if volume > 0 {
            volumeIsMuted = false
        }
        else {
            volumeIsMuted = true
        }

        super.init()
    }

    // MARK: Listening session lifecycle

    /**
     Starts the current listening session and informs the delegate.
     */
    private func startListeningSession(sendData: Bool) {
        sessionIsActive = true
        if let delegate = delegate {
            let sessionListeningChannel = makeSession(sessionEnded: false)
            delegate.listeningSessionDidStart(sessionListeningChannel: sessionListeningChannel,
                                              impressionRadioEvent: nil,
                                              shouldReportData: sendData)
        }
    }

    /**
     Starts the current listening session and informs the delegate, using the supplied radio event.
     */
    private func startListeningSession(_ impressionRadioEvent: ImpressionRadioEvent, shouldReportData: Bool) {
        sessionIsActive = true
        sessionSource = .radioEvent
        self.impressionRadioEvent = impressionRadioEvent

        if let delegate = delegate {
            let sessionListeningChannel = makeSession(sessionEnded: false)
            delegate.listeningSessionDidStart(sessionListeningChannel: sessionListeningChannel,
                                              impressionRadioEvent: impressionRadioEvent,
                                              shouldReportData: shouldReportData)
        }
    }

    /**
     Starts the current listening session and informs the delegate, using the supplied listening session.
     */
    func startListeningSession(_ sessionListeningChannel: SessionListeningChannel, shouldReportData: Bool) {
        sessionIsActive = true
        sessionSource = .listeningSession
        self.sessionListeningChannel = sessionListeningChannel

        delegate?.listeningSessionDidStart(sessionListeningChannel: sessionListeningChannel,
                                           impressionRadioEvent: nil,
                                           shouldReportData: shouldReportData)
    }

    /**
     Ends the current listening session and informs the delegate.
     */
    func stopListeningSession(shouldReportData: Bool) {
        let sessionListeningChannel = makeSession(sessionEnded: true)

        sessionIsActive = false
        sessionSource = .noSession

        delegate?.listeningSessionDidEnd(sessionListeningChannel: sessionListeningChannel,
                                         shouldReportData: shouldReportData)
    }

    /**
     Receives a radio event from the NextRadio reporting SDK.
     */
    func reportingSDKDidReceiveImpressionRadioEvent(_ impressionRadioEvent: ImpressionRadioEvent) {
        switch impressionRadioEvent.deliveryType {
        case DeliveryType.undefined:
            handleUndefinedRadioImpressionEvent(impressionRadioEvent)
        case DeliveryType.FM:
            handleBroadcastRadioImpressionEvent(impressionRadioEvent)
        case DeliveryType.Stream:
            handleStreamingRadioImpressionEvent(impressionRadioEvent)
        case DeliveryType.AM:
            handleBroadcastRadioImpressionEvent(impressionRadioEvent)
        }
    }

    /**
     Receives a radio event with a delivery type of "undefined".
     The event won't have enough information to determine if it should affect an active session, so
     this method will just save the event and notify the delegate.
     */
    private func handleUndefinedRadioImpressionEvent(_ impressionRadioEvent: ImpressionRadioEvent) {
        self.impressionRadioEvent = impressionRadioEvent
        delegate?.radioEventWasReceived(impressionRadioEvent: impressionRadioEvent, shouldReportData: false)
    }

    /**
     Receives a radio event with a delivery type of "FM" or "AM".
     Use the event to determine if it should also start a new session.
     */
    private func handleBroadcastRadioImpressionEvent(_ impressionRadioEvent: ImpressionRadioEvent) {
        switch sessionSource {
        case .noSession:
            startListeningSession(impressionRadioEvent, shouldReportData: true)
        case .listeningSession:
            if shouldRadioEventStartNewListeningSession(lhs: impressionRadioEvent,
                                                        rhs: self.sessionListeningChannel) {
                stopListeningSession(shouldReportData: false)
                startListeningSession(impressionRadioEvent, shouldReportData: true)
            }
            else {
                self.impressionRadioEvent = impressionRadioEvent
                delegate?.radioEventWasReceived(impressionRadioEvent: impressionRadioEvent, shouldReportData: false)
            }
        case .radioEvent:
            if hasImpressionRadioEvent
                && radioEventsAreEqual(lhs: impressionRadioEvent, rhs: self.impressionRadioEvent!) {
                return
            }
            if shouldRadioEventStartNewListeningSession(lhs: impressionRadioEvent,
                                                        rhs: self.impressionRadioEvent) {
                stopListeningSession(shouldReportData: false)
                startListeningSession(impressionRadioEvent, shouldReportData: true)
            }
            else {
                self.impressionRadioEvent = impressionRadioEvent
                delegate?.radioEventWasReceived(impressionRadioEvent: impressionRadioEvent, shouldReportData: false)
            }
        }
    }

    /**
     Receives a radio event with a delivery type of "Stream".
     Host media apps operating in streaming audio mode should be using startListeningSession() and
     stopListeningSession() for session management, as the "radio events" they receive will often not
     contain enough information to determine if it should affect an active session (e.g., Triton
     Digital Player SDK CuePointEvent), so this method will just save the event and notify the delegate.
     */
    private func handleStreamingRadioImpressionEvent(_ impressionRadioEvent: ImpressionRadioEvent) {
        self.impressionRadioEvent = impressionRadioEvent
        delegate?.radioEventWasReceived(impressionRadioEvent: impressionRadioEvent, shouldReportData: false)
    }

    // MARK: SessionListeningChannel data type creation

    /**
     Creates a new listening session NextRadio data type.
     */
    func makeSession(sessionEnded: Bool) -> SessionListeningChannel {
        let session = SessionListeningChannel()
        session.sessionEnded = sessionEnded

        if sessionSource == .radioEvent && hasImpressionRadioEvent {
            session.sessionStartTime = impressionRadioEvent!.creationTime
            session.sessionEndTime = Date()
            session.deliveryType = impressionRadioEvent!.deliveryType
            session.frequencyHz = impressionRadioEvent!.frequencyHz
            session.frequencySubchannel = impressionRadioEvent!.frequencySubchannel
            session.callLetters = impressionRadioEvent!.callLetters
            session.latitude = impressionRadioEvent!.latitude
            session.longitude = impressionRadioEvent!.longitude
        }
        else if sessionSource == .listeningSession && hasSessionListeningChannel {
            session.sessionStartTime = sessionListeningChannel!.sessionStartTime
            session.sessionEndTime = Date()
            session.deliveryType = sessionListeningChannel!.deliveryType
            session.frequencyHz = sessionListeningChannel!.frequencyHz
            session.frequencySubchannel = sessionListeningChannel!.frequencySubchannel
            session.callLetters = sessionListeningChannel!.callLetters
            session.latitude = sessionListeningChannel!.latitude
            session.longitude = sessionListeningChannel!.longitude
        }
        else {
            session.sessionStartTime = Date()
            session.sessionEndTime = session.sessionStartTime
            session.deliveryType = .undefined
            session.frequencyHz = 0
            session.frequencySubchannel = 0
        }

        return session
    }

    /**
     Updates the location used when creating a new listening session NextRadio data type.
     */
    func updateSesionLocation(location: Location?) {
        let latitude = location?.latitude ?? nil
        let longitude = location?.longitude ?? nil

        if hasImpressionRadioEvent {
            impressionRadioEvent!.latitude = latitude
            impressionRadioEvent!.longitude = longitude
        }
        if hasSessionListeningChannel {
            sessionListeningChannel!.latitude = latitude
            sessionListeningChannel!.longitude = longitude
        }
    }

    // MARK: Volume change observation

    func startObservingVolumeChanges() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setActive(true)
            audioSession.addObserver(self,
                                     forKeyPath: VolumeObservation.volumeKey,
                                     options: [.new, .initial],
                                     context: &VolumeObservation.context)
        }
        catch {
            os_log("Unable to activate the audio session", log: OSLog.default, type: .error)
        }

        // This is an alternate way to lsiten for volume changes:
        //var volumeObserver: NSKeyValueObservation?   // make this a class property
        //volumeObserver = audioSession.observe( \.outputVolume ) { (av, change) in
        //print("volume \(av.outputVolume)")
    }

    func stopObservingVolumeChanges() {
        let audioSession = AVAudioSession.sharedInstance()
        audioSession.removeObserver(self,
                                    forKeyPath: VolumeObservation.volumeKey,
                                    context: &VolumeObservation.context)
    }

    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        if (context == &VolumeObservation.context) && (keyPath == VolumeObservation.volumeKey) {
            if let newVolume = change?[NSKeyValueChangeKey.newKey] as? Float {
                didReceiveVolumeChange(newVolume: newVolume)
            }
        }
        else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }

    func didReceiveVolumeChange(newVolume: Float) {
        if newVolume > 0.0 {
            if volumeIsMuted == true {
                volumeIsMuted = false
                if sessionIsActive {
                    startListeningSession(sendData: true)
                }
            }
        }
        else {
            if volumeIsMuted == false {
                volumeIsMuted = true
                if sessionIsActive {
                    stopListeningSession(shouldReportData: true)
                }
            }
        }
    }
}
