//
//  Created by John Koszarek on 12/27/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation
import os.log

public class NextRadioDataCollection: CustomStringConvertible, Archivable {

    public static let documentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    public static let eventDataArchiveUrl = documentsDirectory.appendingPathComponent("tsaCollection")
    public static let transactionEventDataArchiveUrl = documentsDirectory.appendingPathComponent("tsaTransactionCollection")

    enum NextRadioDataCollectionError: Error {
        case unableToArchive
    }

    /** A textual representation of this collection. */
    public var description: String {
        var theDescription = String("event data: ")
        theDescription.append(eventData.description)
        theDescription.append(", transaction event data: ")
        theDescription.append(transactionEventData.description)
        return theDescription
    }

    /** Event data not yet included in a transaction. */
    private var eventData = [NextRadioDataType]()

    /** Event data in a transaction, where the key is the session ID and the value is the event data. */
    private var transactionEventData = [String: [NextRadioDataType]]()

    /**
     Adds the specified TagStationDataType object to this collection.
     */
    public func addEventData(_ nextRadioData: NextRadioDataType) {
        eventData.append(nextRadioData)
        do {
            try saveObject()
        }
        catch {
            os_log("Unable to save event data objects", log: OSLog.default, type: .error)
        }
    }

    /**
     Returns true if this collection contains event data not yet in a transaction.
     */
    public func containsEventData() -> Bool {
        return !eventData.isEmpty ? true : false
    }

    func beginTransaction(transactionId: SessionId) -> [NextRadioDataType]? {
        guard !eventData.isEmpty else {
            return nil
        }

        transactionEventData[transactionId.id] = eventData
        eventData.removeAll()
        saveAllData()

        return transactionEventData[transactionId.id]
    }

    func endTransaction(transactionId: String) {
        guard transactionEventData.index(forKey: transactionId) != nil else {
            return
        }

        transactionEventData.removeValue(forKey: transactionId)
        do {
            try saveTransactionEventsObject()
        }
        catch {
            os_log("Unable to save event transaction data objects", log: OSLog.default, type: .error)
        }
    }

    func rollbackTransaction(transactionId: String) {
        guard transactionEventData.index(forKey: transactionId) != nil else {
            return
        }

        eventData += transactionEventData[transactionId]!
        saveAllData()
    }

    func rollbackTransactions() {
        guard !transactionEventData.isEmpty else {
            return
        }

        for (_, theTransactionEventData) in transactionEventData {
            eventData += theTransactionEventData
        }
        transactionEventData.removeAll()
        saveAllData()
    }

    func loadAllData() {
        if let ed = NextRadioDataCollection.loadObject() as? [NextRadioDataType] {
            eventData = ed
        }
        if let etd = NextRadioDataCollection.loadTransactionEventsObject() as? [String: [NextRadioDataType]] {
            transactionEventData = etd
        }
    }

    private func saveAllData() {
        do {
            try saveObject()
        }
        catch {
            os_log("Unable to save event data objects", log: OSLog.default, type: .error)
        }

        do {
            try saveTransactionEventsObject()
        }
        catch {
            os_log("Unable to save event transaction data objects", log: OSLog.default, type: .error)
        }
    }

    private func deleteAllData() {
        eventData.removeAll()
        do {
            try NextRadioDataCollection.deleteObject()
        }
        catch {
            os_log("Unable to delete event data objects", log: OSLog.default, type: .error)
        }

        transactionEventData.removeAll()
        do {
            try NextRadioDataCollection.deleteTransactionEventsObject()
        }
        catch {
            os_log("Unable to delete event transaction data objects", log: OSLog.default, type: .error)
        }
    }

    // MARK: Archivable protocol

    func saveObject() throws {
        let success = NSKeyedArchiver.archiveRootObject(eventData,
                                                        toFile: NextRadioDataCollection.eventDataArchiveUrl.path)
        if !success {
            throw NextRadioDataCollectionError.unableToArchive
        }
    }

    class func loadObject() -> Any? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: NextRadioDataCollection.eventDataArchiveUrl.path)
    }

    class func deleteObject() throws {
        let exists = FileManager().fileExists(atPath: NextRadioDataCollection.eventDataArchiveUrl.path)
        if exists {
            try FileManager().removeItem(atPath: NextRadioDataCollection.eventDataArchiveUrl.path)
        }
    }

    // MARK: Archive transaction event data

    func saveTransactionEventsObject() throws {
        let success = NSKeyedArchiver.archiveRootObject(transactionEventData,
                                                        toFile: NextRadioDataCollection.transactionEventDataArchiveUrl.path)
        if !success {
            throw NextRadioDataCollectionError.unableToArchive
        }
    }

    class func loadTransactionEventsObject() -> Any? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: NextRadioDataCollection.transactionEventDataArchiveUrl.path)
    }

    class func deleteTransactionEventsObject() throws {
        let exists = FileManager().fileExists(atPath: NextRadioDataCollection.transactionEventDataArchiveUrl.path)
        if exists {
            try FileManager().removeItem(atPath: NextRadioDataCollection.transactionEventDataArchiveUrl.path)
        }
    }
}
