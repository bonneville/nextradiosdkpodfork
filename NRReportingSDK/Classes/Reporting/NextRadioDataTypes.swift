//
//  Data types used with the NextRadio reporting SDK.
//
//  Created by John Koszarek on 12/4/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation
import CoreLocation

/// Allows NextRadio reporting SDK data objects to be archived and to be converted into a Dictionary.
@objc
public protocol NextRadioDataType: NSCoding {

    /// Returns a NextRadio Reporting SDK data object as a Dictionary.
    func toDictionary() -> [String : Any]
}

/// Allows the latitude and longitude to be set on a NextRadio reporting SDK data object.
@objc
public protocol Locationable {

    /// Sets latitude and longitude using the supplied CLLocation object.
    func setLocation(location: CLLocation)
}

@objc
public enum DeliveryType: Int {
    case undefined = 0
    case FM = 1
    case Stream = 2
    case AM = 3
}

// MARK: ImpressionRadioEvent class

@objc
public class ImpressionRadioEvent: NSObject, NextRadioDataType, Locationable {

    static let typeKey = "type"
    static let creationTimeKey = "createTime"
    static let artistKey = "artist"
    static let titleKey = "title"
    static let eventMetadataKey = "eventMetadata"
    static let deliveryTypeKey = "deliveryType"
    static let frequencyHzKey = "frequencyHz"
    static let frequencySubchannelKey = "frequencySubChannel"
    static let callLettersKey = "callLetters"
    static let latitudeKey = "latitude"
    static let longitudeKey = "longitude"

    @objc public static let type = "Impression.RadioEvent"

    // Creation time of this record (required)
    var creationTime: Date  // device time

    // Radio event description (required)
    @objc public var artist: String
    @objc public var title: String
    @objc public var eventMetadata: String

    // Station identifiers (required)
    @objc public var deliveryType: DeliveryType
    @objc public var frequencyHz: Int
    @objc public var frequencySubchannel: Int

    // GPS data (optional)
    public var latitude: Double?
    public var longitude: Double?

    // Optional
    @objc public var callLetters: String?

    /** Creates a new ImpressionRadioEvent object. */
    @objc
    public override convenience init() {
        let creationTime = Date()
        self.init(creationTime: creationTime, artist: "", title: "", eventMetadata: "", deliveryType: .undefined,
                  frequencyHz: 0, frequencySubchannel: 0, latitude: nil, longitude: nil, callLetters: nil)
    }

    /** Creates a new ImpressionRadioEvent object. */
    @objc
    public convenience init(artist: String, title: String, deliveryType: DeliveryType, frequencyHz: Int, frequencySubchannel: Int = 0) {
        let creationTime = Date()
        self.init(creationTime: creationTime, artist: artist, title: title, eventMetadata: "", deliveryType: deliveryType,
                  frequencyHz: frequencyHz, frequencySubchannel: frequencySubchannel, latitude: nil, longitude: nil,
                  callLetters: nil)
    }

    /** Creates a new ImpressionRadioEvent object. */
    @objc
    public convenience init(eventMetadata: String, deliveryType: DeliveryType, frequencyHz: Int, frequencySubchannel: Int = 0) {
        let creationTime = Date()
        self.init(creationTime: creationTime, artist: "", title: "", eventMetadata: eventMetadata, deliveryType: deliveryType,
                  frequencyHz: frequencyHz, frequencySubchannel: frequencySubchannel, latitude: nil, longitude: nil,
                  callLetters: nil)
    }

    /**
     Creates a new ImpressionRadioEvent object.
     This method is intended for use when unarchiving a previously saved ImpressionRadioEvent.
     */
    init(creationTime: Date, artist: String, title: String, eventMetadata: String, deliveryType: DeliveryType,
         frequencyHz: Int, frequencySubchannel: Int, latitude: Double?, longitude: Double?, callLetters: String?) {
        self.creationTime = creationTime
        self.artist = artist
        self.title = title
        self.eventMetadata = eventMetadata
        self.deliveryType = deliveryType
        self.frequencyHz = frequencyHz
        self.frequencySubchannel = frequencySubchannel
        self.latitude = latitude
        self.longitude = longitude
        self.callLetters = callLetters
    }

    @objc public override var description: String {
        return toDictionary().description
    }

    @objc public override var debugDescription: String {
        return toDictionary().debugDescription
    }

    // MARK: NSCoding protocol for ImpressionRadioEvent class

    @objc
    public required convenience init?(coder aDecoder: NSCoder) {
        // If we can't decode a required property the initializer should fail.
        guard let ct = aDecoder.decodeObject(forKey: ImpressionRadioEvent.creationTimeKey) as? Date else {
            return nil
        }
        guard let dt = aDecoder.decodeObject(forKey: ImpressionRadioEvent.deliveryTypeKey) as? DeliveryType else {
            return nil
        }
        guard let fh = aDecoder.decodeObject(forKey: ImpressionRadioEvent.frequencyHzKey) as? Int else {
            return nil
        }
        guard let fs = aDecoder.decodeObject(forKey: ImpressionRadioEvent.frequencySubchannelKey) as? Int else {
            return nil
        }

        // Required properties, but TagStation technically doesn't require all of them (e.g., artist
        // and title but no event metadata is OK). We won't fail the initializer if they are missing
        // because of that.
        let a = aDecoder.decodeObject(forKey: ImpressionRadioEvent.artistKey) as? String ?? ""
        let t = aDecoder.decodeObject(forKey: ImpressionRadioEvent.titleKey) as? String ?? ""
        let em = aDecoder.decodeObject(forKey: ImpressionRadioEvent.eventMetadataKey) as? String ?? ""

        // Optional properties.
        let lat = aDecoder.decodeObject(forKey: ImpressionRadioEvent.latitudeKey) as? Double
        let long = aDecoder.decodeObject(forKey: ImpressionRadioEvent.longitudeKey) as? Double
        let cl = aDecoder.decodeObject(forKey: ImpressionRadioEvent.callLettersKey) as? String

        self.init(creationTime: ct, artist: a, title: t, eventMetadata: em, deliveryType: dt, frequencyHz: fh,
                  frequencySubchannel: fs, latitude: lat, longitude: long, callLetters: cl)
    }

    @objc
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(creationTime, forKey: ImpressionRadioEvent.creationTimeKey)
        aCoder.encode(artist, forKey: ImpressionRadioEvent.artistKey)
        aCoder.encode(title, forKey: ImpressionRadioEvent.titleKey)
        aCoder.encode(eventMetadata, forKey: ImpressionRadioEvent.eventMetadataKey)
        aCoder.encode(deliveryType.rawValue, forKey: ImpressionRadioEvent.deliveryTypeKey)
        aCoder.encode(frequencyHz, forKey: ImpressionRadioEvent.frequencyHzKey)
        aCoder.encode(frequencySubchannel, forKey: ImpressionRadioEvent.frequencySubchannelKey)
        aCoder.encode(callLetters, forKey: ImpressionRadioEvent.callLettersKey)
        aCoder.encode(latitude, forKey: ImpressionRadioEvent.latitudeKey)
        aCoder.encode(longitude, forKey: ImpressionRadioEvent.longitudeKey)
    }

    // MARK: NextRadioDataType protocol for ImpressionRadioEvent class

    @objc
    public func toDictionary() -> [String : Any] {
        var values = [String : Any]()

        values[ImpressionRadioEvent.typeKey] = ImpressionRadioEvent.type
        values[ImpressionRadioEvent.creationTimeKey] = creationTime.format()
        values[ImpressionRadioEvent.artistKey] = artist
        values[ImpressionRadioEvent.titleKey] = title
        values[ImpressionRadioEvent.eventMetadataKey] = eventMetadata
        values[ImpressionRadioEvent.deliveryTypeKey] = deliveryType.rawValue
        values[ImpressionRadioEvent.frequencyHzKey] = frequencyHz
        values[ImpressionRadioEvent.frequencySubchannelKey] = frequencySubchannel
        if let latitude = latitude {
            values[ImpressionRadioEvent.latitudeKey] = latitude
        }
        if let longitude = longitude {
            values[ImpressionRadioEvent.longitudeKey] = longitude
        }
        if let callLetters = callLetters {
            values[ImpressionRadioEvent.callLettersKey] = callLetters
        }

        return values
    }

    // MARK: Locationable protocol for ImpressionRadioEvent class

    @objc
    public func setLocation(location: CLLocation) {
        latitude = location.coordinate.latitude
        longitude = location.coordinate.longitude
    }
}

// MARK: Location class

@objc
public class Location: NSObject, NextRadioDataType, Locationable  {

    static let typeKey = "type"
    static let creationTimeKey = "createTime"
    static let latitudeKey = "latitude"
    static let longitudeKey = "longitude"
    static let gpsUtcTimeKey = "gpsUtcTime"
    static let deviceSpeedKey = "speed"
    static let horizontalAccuracyKey = "hAccuracy"
    static let ipAddressKey = "ipAddress"

    @objc public static let type = "Location"

    // Creation time of this record (required)
    var creationTime: Date          // device time

    // GPS data (required)
    public var latitude: Double
    public var longitude: Double

    // Optional
    @objc public var gpsUtcTime: Date?    // GPS time
    public var deviceSpeed: Double?
    public var horizontalAccuracy: Double?
    @objc public var ipAddress: String?

    /** Creates a new Location object. */
    @objc
    public convenience init(latitude: Double, longitude: Double) {
        let creationTime = Date()
        self.init(creationTime: creationTime, latitude: latitude, longitude: longitude, gpsUtcTime: nil,
                  deviceSpeed: nil, horizontalAccuracy: nil, ipAddress: nil)
    }

    /**
     Creates a new Location object.
     This method is intended for use when unarchiving a previously saved Location.
     */
    init(creationTime: Date, latitude: Double, longitude: Double, gpsUtcTime: Date?, deviceSpeed: Double?,
         horizontalAccuracy: Double?, ipAddress: String?) {
        self.creationTime = creationTime
        self.latitude = latitude
        self.longitude = longitude
        self.gpsUtcTime = gpsUtcTime
        self.deviceSpeed = deviceSpeed
        self.horizontalAccuracy = horizontalAccuracy
        self.ipAddress = ipAddress
    }

    @objc public override var description: String {
        return toDictionary().description
    }

    @objc public override var debugDescription: String {
        return toDictionary().debugDescription
    }

    // NSCoding protocol for Location class

    @objc
    public required convenience init?(coder aDecoder: NSCoder) {
        // If we can't decode a required property the initializer should fail.
        guard let ct = aDecoder.decodeObject(forKey: Location.creationTimeKey) as? Date else {
            return nil
        }
        guard let lat = aDecoder.decodeObject(forKey: Location.latitudeKey) as? Double else {
            return nil
        }
        guard let long = aDecoder.decodeObject(forKey: Location.longitudeKey) as? Double else {
            return nil
        }

        // Optional properties.
        let gut = aDecoder.decodeObject(forKey: Location.gpsUtcTimeKey) as? Date
        let ds = aDecoder.decodeObject(forKey: Location.deviceSpeedKey) as? Double
        let ha = aDecoder.decodeObject(forKey: Location.gpsUtcTimeKey) as? Double
        let ia = aDecoder.decodeObject(forKey: Location.gpsUtcTimeKey) as? String

        self.init(creationTime: ct, latitude: lat, longitude: long, gpsUtcTime: gut, deviceSpeed: ds,
                  horizontalAccuracy: ha, ipAddress: ia)
    }

    @objc
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(creationTime, forKey: Location.creationTimeKey)
        aCoder.encode(latitude, forKey: Location.latitudeKey)
        aCoder.encode(longitude, forKey: Location.longitudeKey)
        aCoder.encode(gpsUtcTime, forKey: Location.gpsUtcTimeKey)
        aCoder.encode(deviceSpeed, forKey: Location.deviceSpeedKey)
        aCoder.encode(horizontalAccuracy, forKey: Location.horizontalAccuracyKey)
        aCoder.encode(ipAddress, forKey: Location.ipAddressKey)
    }

    // MARK: NextRadioDataType protocol for Location class

    @objc
    public func toDictionary() -> [String : Any] {
        var values = [String : Any]()

        values[Location.typeKey] = Location.type
        values[Location.creationTimeKey] = creationTime.format()
        values[Location.latitudeKey] = latitude
        values[Location.longitudeKey] = longitude
        if let gpsUtcTime = gpsUtcTime {
            values[Location.gpsUtcTimeKey] = gpsUtcTime.format()
        }
        if let deviceSpeed = deviceSpeed {
            values[Location.deviceSpeedKey] = deviceSpeed
        }
        if let horizontalAccuracy = horizontalAccuracy {
            values[Location.horizontalAccuracyKey] = horizontalAccuracy
        }
        if let ipAddress = ipAddress {
            values[Location.ipAddressKey] = ipAddress
        }

        return values
    }

    // MARK: Locationable protocol for Location class

    @objc
    public func setLocation(location: CLLocation) {
        latitude = location.coordinate.latitude
        longitude = location.coordinate.longitude
    }
}

// MARK: SessionListeningChannel class

@objc
public class SessionListeningChannel: NSObject, NextRadioDataType, Locationable  {

    static let typeKey = "type"
    static let sessionStartTimeKey = "startTime"
    static let sessionEndTimeKey = "endTime"
    static let deliveryTypeKey = "deliveryType"
    static let frequencyHzKey = "frequencyHz"
    static let frequencySubchannelKey = "frequencySubChannel"
    static let sessionEndedKey = "isEnded"
    static let callLettersKey = "callLetters"
    static let latitudeKey = "latitude"
    static let longitudeKey = "longitude"

    @objc public static let type = "Session.Listening.Channel"

    // Session start and end times (required)
    var sessionStartTime: Date  // device time
    var sessionEndTime: Date    // device time

    // Station identifiers (required)
    @objc public var deliveryType: DeliveryType
    @objc public var frequencyHz: Int
    @objc public var frequencySubchannel: Int

    // GPS data (optional)
    public var latitude: Double?
    public var longitude: Double?

    // Optional
    @objc public var callLetters: String?
    public var sessionEnded: Bool?

    /** Creates a new SessionListeningChannel object. */
    @objc
    public override convenience init() {
        let sessionTime = Date()    // note we are using this date for both the session start time and end times
        self.init(sessionStartTime: sessionTime, sessionEndTime: sessionTime, deliveryType: .undefined, frequencyHz: 0,
                  frequencySubchannel: 0, latitude: nil, longitude: nil, callLetters: nil, sessionEnded: false)
    }

    /** Creates a new SessionListeningChannel object. */
    @objc
    public convenience init(deliveryType: DeliveryType, frequencyHz: Int, frequencySubchannel: Int = 0,
                            callLetters: String? = "") {
        let sessionTime = Date()    // note we are using this date for both the session start time and end times
        self.init(sessionStartTime: sessionTime, sessionEndTime: sessionTime, deliveryType: deliveryType,
                  frequencyHz: frequencyHz, frequencySubchannel: frequencySubchannel, latitude: nil,
                  longitude: nil, callLetters: callLetters, sessionEnded: false)
    }

    /**
     Creates a new SessionListeningChannel object.
     This method is intended for use when unarchiving a previously saved SessionListeningChannel.
     */
    init(sessionStartTime: Date, sessionEndTime: Date, deliveryType: DeliveryType, frequencyHz: Int,
         frequencySubchannel: Int, latitude: Double?, longitude: Double?, callLetters: String?,
         sessionEnded: Bool? = false) {
        self.sessionStartTime = sessionStartTime
        self.sessionEndTime = sessionEndTime
        self.deliveryType = deliveryType
        self.frequencyHz = frequencyHz
        self.frequencySubchannel = frequencySubchannel
        self.latitude = latitude
        self.longitude = longitude
        self.callLetters = callLetters
        self.sessionEnded = sessionEnded
    }

    @objc public override var description: String {
        return toDictionary().description
    }

    @objc public override var debugDescription: String {
        return toDictionary().debugDescription
    }

    // MARK: NSCoding protocol for SessionListeningChannel class

    @objc
    public required convenience init?(coder aDecoder: NSCoder) {
        // If we can't decode a required property the initializer should fail.
        guard let ssTime = aDecoder.decodeObject(forKey: SessionListeningChannel.sessionStartTimeKey) as? Date else {
            return nil
        }
        guard let seTime = aDecoder.decodeObject(forKey: SessionListeningChannel.sessionEndTimeKey) as? Date else {
            return nil
        }
        guard let dt = aDecoder.decodeObject(forKey: SessionListeningChannel.deliveryTypeKey) as? DeliveryType else {
            return nil
        }
        guard let fh = aDecoder.decodeObject(forKey: SessionListeningChannel.frequencyHzKey) as? Int else {
            return nil
        }
        guard let fs = aDecoder.decodeObject(forKey: SessionListeningChannel.frequencySubchannelKey) as? Int else {
            return nil
        }

        // Optional properties.
        let lat = aDecoder.decodeObject(forKey: SessionListeningChannel.latitudeKey) as? Double
        let long = aDecoder.decodeObject(forKey: SessionListeningChannel.longitudeKey) as? Double
        let cl = aDecoder.decodeObject(forKey: SessionListeningChannel.callLettersKey) as? String
        let se = aDecoder.decodeBool(forKey: SessionListeningChannel.sessionEndedKey)

        self.init(sessionStartTime: ssTime, sessionEndTime: seTime, deliveryType: dt, frequencyHz: fh,
                  frequencySubchannel: fs, latitude: lat, longitude: long, callLetters: cl, sessionEnded: se)
    }

    @objc
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(sessionStartTime, forKey: SessionListeningChannel.sessionStartTimeKey)
        aCoder.encode(sessionEndTime, forKey: SessionListeningChannel.sessionEndTimeKey)
        aCoder.encode(deliveryType.rawValue, forKey: SessionListeningChannel.deliveryTypeKey)
        aCoder.encode(frequencyHz, forKey: SessionListeningChannel.frequencyHzKey)
        aCoder.encode(frequencySubchannel, forKey: SessionListeningChannel.frequencySubchannelKey)
        aCoder.encode(latitude, forKey: SessionListeningChannel.latitudeKey)
        aCoder.encode(longitude, forKey: SessionListeningChannel.longitudeKey)
        aCoder.encode(callLetters, forKey: SessionListeningChannel.callLettersKey)
        aCoder.encode(sessionEnded, forKey: SessionListeningChannel.sessionEndedKey)
    }

    // MARK: NextRadioDataType protocol for SessionListeningChannel class

    @objc
    public func toDictionary() -> [String : Any] {
        var values = [String : Any]()

        values[SessionListeningChannel.typeKey] = SessionListeningChannel.type
        values[SessionListeningChannel.sessionStartTimeKey] = sessionStartTime.format()
        values[SessionListeningChannel.sessionEndTimeKey] = sessionEndTime.format()
        values[SessionListeningChannel.deliveryTypeKey] = deliveryType.rawValue
        values[SessionListeningChannel.frequencyHzKey] = frequencyHz
        values[SessionListeningChannel.frequencySubchannelKey] = frequencySubchannel
        if let latitude = latitude {
            values[SessionListeningChannel.latitudeKey] = latitude
        }
        if let longitude = longitude {
            values[SessionListeningChannel.longitudeKey] = longitude
        }
        if let callLetters = callLetters {
            values[SessionListeningChannel.callLettersKey] = callLetters
        }
        if let sessionEnded = sessionEnded {
            values[SessionListeningChannel.sessionEndedKey] = sessionEnded
        }

        return values
    }

    // MARK: Locationable protocol for SessionListeningChannel class

    @objc
    public func setLocation(location: CLLocation) {
        latitude = location.coordinate.latitude
        longitude = location.coordinate.longitude
    }
}

// MARK: UtcOffset class

@objc
public class UtcOffset: NSObject, NextRadioDataType  {

    static let typeKey = "type"
    static let deviceTimeKey = "clientRequestTime"

    @objc public static let type = "Utc.Offset"

    var deviceTime: Date

    /** Creates a new UtcOffset object. */
    @objc
    public override init() {
        self.deviceTime = Date()
    }

    /**
     Creates a new UtcOffset object.
     This method is intended for use when unarchiving a previously saved UtcOffset.
     */
    init(deviceTime: Date) {
        self.deviceTime = deviceTime
    }

    @objc public override var description: String {
        return toDictionary().description
    }

    @objc public override var debugDescription: String {
        return toDictionary().debugDescription
    }

    // MARK: NSCoding protocol for UtcOffset class

    @objc
    public required convenience init?(coder aDecoder: NSCoder) {
        // If we can't decode a required property the initializer should fail.
        guard let dt = aDecoder.decodeObject(forKey: UtcOffset.deviceTimeKey) as? Date else {
            return nil
        }
        self.init(deviceTime: dt)
    }

    @objc
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(deviceTime, forKey: UtcOffset.deviceTimeKey)
    }

    // MARK: NextRadioDataType protocol for UtcOffset class

    @objc
    public func toDictionary() -> [String : Any] {
        var values = [String : Any]()

        values[UtcOffset.typeKey] = UtcOffset.type
        values[UtcOffset.deviceTimeKey] = deviceTime.format()

        return values
    }
}
