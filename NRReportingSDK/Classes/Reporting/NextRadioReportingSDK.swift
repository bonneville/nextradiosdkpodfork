//
//  Created by John Koszarek on 12/1/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation
import CoreLocation
import os.log

/// NextRadio reporting business logic.
///
/// This is the class that reporting clients should use, both to register a device and to report
/// analytics event data to TagStation.
///
/// Event data is sent to TagStation when:
/// - A new listening sessions begins or an existing session ends.
/// - On a heartbeat "tick" (currently this is set to 2 minutes).
/// - The host media app moves to the background or is terminated.
///   Note: the host media app does this by calling: deactivateReportingSDK()
@objc
public final class NextRadioReportingSDK: NSObject, DeviceRegistrationDelegate, ListeningSessionDelegate
{
    @objc
    public static let sharedInstance = NextRadioReportingSDK()

    // True if we should use the TagStation development environment, false if we should use the
    // production environment.
    @objc
    public var isDevelopmentEnvironment = false {
        didSet {
            deviceRegistration.isDevelopmentEnvironment = isDevelopmentEnvironment
        }
    }

    // Timer used as the NextRadio Reporting SDK heartbeat. It determines when data is sent to
    // TagStation.
    var heartbeatTimer: Timer?

    // Registers a device, such as an iPhone.
    let deviceRegistration = DeviceRegistration()

    // Storage for analytics event data.
    let eventData = NextRadioDataCollection()

    // Sends JSON data to a TagStation service endpoint.
    let reportingGateway = ReportingGateway()

    /** Creates a new NextRadioReportingSDK object and initializes delegates. */
    private override init() {
        super.init()
        deviceRegistration.delegate = self
        ListeningSession.sharedInstance.delegate = self
    }

    /** Clears up the NextRadio reporting SDK state. */
    deinit {
        cleanupSDK()
    }

    /**
     Cleans up the NextRadio reporting SDK state.
     - Sends any event data still stored on the device to TagStation.
     This should be called when the host media app moves to the background or is terminated.

     Parameter applicationWillTerminate: If true, the app is terminating as opposed to entering the
     background. If there is an active listening session, it will be ended.
     */
    private func cleanupSDK(applicationWillTerminate: Bool = false) {
        //ListeningSession.sharedInstance.stopObservingVolumeChanges()
        if ListeningSession.sharedInstance.sessionIsActive && applicationWillTerminate {
            heartbeatTimer?.invalidate()
            ListeningSession.sharedInstance.stopListeningSession(shouldReportData: false)
        }
        reportNextRadioData()
    }

    /**
     Cleans up the NextRadio reporting SDK's saved data.
     */
    @objc
    public func cleanupAllData() {
        do {
            try NextRadioReportingKey.deleteObject()
            try DeviceRegistrationData.deleteObject()
            try TagStationDeviceId.deleteObject()
            try NextRadioDataCollection.deleteObject()
        }
        catch NextRadioReportingKey.NextRadioReportingKeyError.unableToDelete {
            os_log("Unable to delete device registration data", log: OSLog.default, type: .error)
        }
        catch DeviceRegistrationData.DeviceRegistrationDataError.unableToDelete {
            os_log("Unable to delete device registration data", log: OSLog.default, type: .error)
        }
        catch TagStationDeviceId.TagStationDeviceIdError.unableToDelete {
            os_log("Unable to delete TagStation device ID", log: OSLog.default, type: .error)
        }
        catch {
            os_log("Unable to delete NextRadio SDK data", log: OSLog.default, type: .error)
        }
    }

    // MARK: Application state transitions

    /**
     Tells the NextRadio Reporting SDK that the app has launched and the SDK should be initialized.
     This will:
     - Rollback any event data transactions that didn't complete (if the device had been previously
       registered and the SDK had recorded event data).
     - Register a device if it has never been registered before, or if its registration data has
       changed.

     Parameter nextRadioReportingKey: The name of a host media app as it identifies itself to TagStation.
     */
    @objc
    public func initReportingSDK(nextRadioReportingKey: String) {
        os_log(">>>> NextRadio SDK: initReportingSDK()", log: OSLog.default, type: .info)

        let reportingKey = NextRadioReportingKey(key: nextRadioReportingKey)
        saveNextRadioReportingKey(nextRadioReportingKey: reportingKey)

        let registration = shouldRegister(nextRadioReportingKey: nextRadioReportingKey)
        if !registration.shouldRegister {
            eventData.loadAllData()
            eventData.rollbackTransactions()
        }
        else {
            registerDevice(nextRadioReportingKey: nextRadioReportingKey)
        }
    }

    /**
     Tells the NextRadio Reporting SDK that the app has become active. This will:
     - Start a "heartbeat" timer for scheduled activities, such as sending event data to TagStation.
     - Create a device time ("UTC offset") event that will be sent to TagStation.
     */
    @objc
    public func activateReportingSDK() {
        os_log(">>>> NextRadio SDK: activateReportingSDK()", log: OSLog.default, type: .info)

        ListeningSession.sharedInstance.startObservingVolumeChanges()

        heartbeatTimer?.invalidate()
        heartbeatTimer = Timer.scheduledTimer(timeInterval: FrameworkConstants.heartbeatTime,
                                              target: self,
                                              selector: #selector(heartbeat),
                                              userInfo: nil,
                                              repeats: true)

        let utcOffset = UtcOffset()
        eventData.addEventData(utcOffset)
    }

    /**
     Tells the NextRadio Reporting SDK that the app is now in the background or about to terminate.
     This will:
     - Send any event data still stored on the device to TagStation.
     On termination:
     - Stop the "heartbeat" timer for scheduled activities, such as sending event data to TagStation.
     - End an active listening session.

     Parameter applicationWillTerminate: If true, the app is terminating as opposed to entering the
     background.
     */
    @objc
    public func deactivateReportingSDK(applicationWillTerminate: Bool = false) {
        os_log("<<<< NextRadio SDK: deactivateReportingSDK()", log: OSLog.default, type: .info)
        cleanupSDK()
    }

    // MARK: Report data to TagStation

    /**
     Adds a radio event to the list of analytics data that will be sent to TagStation.
     This may also start a new listening session (for instance, if the radio event has a different
     frequency, subchannel, and delivery type as the last radio event received).
     */
    @objc
    public func reportImpressionRadioEvent(_ impressionRadioEvent: ImpressionRadioEvent) {
        if impressionRadioEvent.latitude == nil && impressionRadioEvent.longitude == nil {
            setLocationOnNextRadioData(impressionRadioEvent)
        }
        ListeningSession.sharedInstance.reportingSDKDidReceiveImpressionRadioEvent(impressionRadioEvent)
    }

    /**
     Adds an event indicating that a listening session has begun ended to the list of events that
     will be sent to TagStation.
     Use this method when we want to report that a new session as begun, but we don't have enough
     information to construct a ImpressionRadioEvent object (e.g., we're missing artist and title
     or event metadata).
     */
    @objc
    public func startListeningSession(_ sessionListeningChannel: SessionListeningChannel) {
        if sessionListeningChannel.latitude == nil && sessionListeningChannel.longitude == nil {
            setLocationOnNextRadioData(sessionListeningChannel)
        }
        if ListeningSession.sharedInstance.sessionIsActive {
            ListeningSession.sharedInstance.stopListeningSession(shouldReportData: false)
        }
        ListeningSession.sharedInstance.startListeningSession(sessionListeningChannel, shouldReportData: true)
    }

    /**
     If there is an active listening session, this method adds an event indicating that a listening
     session has ended to the list of events that will be sent to TagStation.
     */
    @objc
    public func stopListeningSession() {
        if ListeningSession.sharedInstance.sessionIsActive {
            ListeningSession.sharedInstance.stopListeningSession(shouldReportData: true)
        }
    }

    /** Adds radio event data to the list of events that will be sent to TagStation. */
    @objc
    public func addImpressionRadioEvent(_ impressionRadioEvent: ImpressionRadioEvent) {
        eventData.addEventData(impressionRadioEvent)
    }

    /** Adds listening session data to the list of events that will be sent to TagStation. */
    @objc
    public func addListeningSession(_ sessionListeningChannel: SessionListeningChannel) {
        eventData.addEventData(sessionListeningChannel)
    }

    /** Adds UTC offset (device time) data to the list of events that will be sent to TagStation. */
    @objc
    public func addUtcOffset(_ utcOffset: UtcOffset) {
        eventData.addEventData(utcOffset)
    }

    /** Adds location data to the list of events that will be sent to TagStation. */
    @objc
    public func addLocation(_ location: Location) {
        eventData.addEventData(location)
    }

    /** Adds location data to the list of events that will be sent to TagStation. */
    @objc
    public func addLocation() {
        let location = DeviceLocation.sharedInstance.makeLocation()
        if let location = location {
            eventData.addEventData(location)
        }
    }

    /**
     Reports an array of NextRadioDataType objects to TagStation.
     */
    private func reportNextRadioData() {
        // Make sure the app is connected to a network.
        guard Reachability.isConnectedToNetwork() else {
            os_log("Network conenction unavailable", log: OSLog.default, type: .info)
            return
        }

        // Make sure the app has a TagStation ID (required when reporting analytics event data to TagStation).
        let tagStationDeviceId = TagStationDeviceId.loadObject() as? TagStationDeviceId
        guard tagStationDeviceId != nil else {
            // If this device have an ID register again. Report event data next time (e.g., next heartbeat).
            os_log("Missing TagStation device ID: Device registration required", log: OSLog.default, type: .error)
            registerDevice()
            return
        }

        // Make sure the app has NextRadio event data to report.
        guard eventData.containsEventData() else {
            os_log("No NextRadio event data to report", log: OSLog.default, type: .debug)
            return
        }

        // Begin a transaction in which we will send event data to TagStation.
        let sessionId = SessionId()
        let nextRadioData = eventData.beginTransaction(transactionId: sessionId)!

        // Convert the event data to JSON.
        let dataIntake = DataIntakeJSON(array: nextRadioData)
        let data = dataIntake.asData()
        guard data != nil else {
            // If the event data couldn't be converted to JSON, there is a bug in our code.
            os_log("Unable to convert analytics events to JSON", log: OSLog.default, type: .error)
            return
        }
        if isDevelopmentEnvironment {
            if let text = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as String? {
                print("JSON: \(String(describing: text))")
            }
        }

        // Send the event data to TagStation.
        let endpoint = ServiceEndpoints.DataIntake.serviceEndpoint(isDevelopmentEnvironment: isDevelopmentEnvironment)
        reportingGateway.dataTask(serviceEndpoint: endpoint,
                                  sessionId: sessionId,
                                  jsonData: data!,
                                  tagStationDeviceId: tagStationDeviceId!,
                                  completionHandler: { (data, response, error) -> Void in
            let sdkSessionIdValue = ReportingGateway.sdkSessionIdValue(response: response)
            if response != nil {
                if let sdkSessionIdValue = sdkSessionIdValue {
                    if let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == NetworkConstants.successResponseCode {
                            os_log("Reporting event data to TagStation succeeded; ending transaction",
                                   log: OSLog.default, type: .debug)
                            self.eventData.endTransaction(transactionId: sdkSessionIdValue)
                        }
                        else {
                            os_log("Reporting event data to TagStation failed; rolling back transaction",
                                   log: OSLog.default, type: .error)
                            self.eventData.rollbackTransaction(transactionId: sdkSessionIdValue)
                        }
                    }
                }
                if self.isDevelopmentEnvironment {
                    print("Response: \(response.debugDescription)")
                }
            }
            if error != nil {
                if let sdkSessionIdValue = sdkSessionIdValue {
                    os_log("Reporting event data to TagStation failed; rolling back transaction",
                           log: OSLog.default, type: .error)
                    self.eventData.rollbackTransaction(transactionId: sdkSessionIdValue)
                }
                if self.isDevelopmentEnvironment {
                    print("Error: \(error.debugDescription)")
                }
            }
        })
    }

    // MARK: Registration

    /**
     Registers a device if it has never been registered before, or if its registration data has changed.
     */
    private func registerDevice() {
        if let registrationKey = NextRadioReportingKey.loadObject() as? NextRadioReportingKey {
            registerDevice(nextRadioReportingKey: registrationKey.key!)
        }
        else {
            os_log("Missing NextRadio reporting key", log: OSLog.default, type: .error)
        }
    }

    /**
     Registers a device if it has never been registered before, or if its registration data has changed.

     Parameter nextRadioReportingKey: The name of a host media app as it identifies itself to TagStation.
     */
    @objc
    public func registerDevice(nextRadioReportingKey: String) {
        let register = shouldRegister(nextRadioReportingKey: nextRadioReportingKey)
        if register.shouldRegister {
            // Delete any existing registeration data.
            try? DeviceRegistrationData.deleteObject()

            // Get data to be used during device registration, such as brand/client/device name, etc.,
            // and archive it.
            let registrationData = register.usingData!
            saveDeviceRegistrationData(deviceRegistrationData: registrationData)

            // Get the unique ID TagStation uses to indentify a device. May be nil if this is the
            // first time this device is being registered.
            let tagStationDeviceId = TagStationDeviceId.loadObject() as? TagStationDeviceId

            // Register this device with TagStation.
            deviceRegistration.registerDevice(registrationData: registrationData,
                                              tagStationDeviceId: tagStationDeviceId)
        }
    }

    /**
     Returns true if this device has never been registered, or if its registration data has changed.
     If true is returned, the device registration data to be used is also returned.
     If false is returned, the device registration data is nil.
     */
    //@objc // Won't compile: Method cannot be marked @objc because its result type cannot be represented in Objective-C
    public func shouldRegister(nextRadioReportingKey: String) -> (shouldRegister: Bool, usingData: DeviceRegistrationData?) {
        let archivedRegistrationData = DeviceRegistrationData.loadObject() as? DeviceRegistrationData
        let registrationData = DeviceRegistrationData(nextRadioReportingKey: nextRadioReportingKey)

        if let archivedRegistrationData = archivedRegistrationData {
            if !archivedRegistrationData.isEqual(to: registrationData) {
                // This device's registration data has changed -- it needs to be registered again.
                os_log("Update device registration", log: OSLog.default, type: .debug)
                return (true, registrationData)
            }
            else {
                // This device has already been registered, and its registration data has not changed --
                // it does not need to be registered again.
                os_log("Device already registred", log: OSLog.default, type: .debug)
                return (false, nil)
            }
        }
        else {
            // This device has never been registered before -- it needs to be registered.
            os_log("New device registration", log: OSLog.default, type: .debug)
            return (true, registrationData)
        }
    }

    // MARK: DeviceRegistrationDelegate protocol

    /**
     Called when a TagStation device ID is obtained during device registration.
     */
    @objc
    public func didReceiveTagStationDeviceId(tagStationDeviceId: TagStationDeviceId) {
        if deviceRegistration.deviceRegistrationStatus == .success {
            if isDevelopmentEnvironment {
                print("Received TagStation ID: \(tagStationDeviceId.id!)")
            }
            saveTagStationDeviceId(tagStationDeviceId: tagStationDeviceId)
        }
    }

    // MARK: ListeningSessionDelegate protocol

    /**
     Called when a listening session started.

     - Parameter sessionListeningChannel: The listening session that was started.
     - Parameter impressionRadioEvent: The radio event received.
     - Parameter shouldReportData: If true, the method implementation should also send the event data to
       TagStation
     */
    @objc
    public func listeningSessionDidStart(sessionListeningChannel: SessionListeningChannel,
                                  impressionRadioEvent: ImpressionRadioEvent?,
                                  shouldReportData: Bool) {
        addListeningSession(sessionListeningChannel)
        if let impressionRadioEvent = impressionRadioEvent {
            addImpressionRadioEvent(impressionRadioEvent)
        }
        if shouldReportData {
            reportNextRadioData()
        }
    }

    /**
     Called when a listening session ended. Adds listening session data to the list of events that
     will be sent to TagStation.

     - Parameter sessionListeningChannel: The listening session event data to be sent.
     - Parameter shouldSendData: If true, send the event data to TagStation now. If false, just add
       it to this list.
     */
    @objc
    public func listeningSessionDidEnd(sessionListeningChannel: SessionListeningChannel, shouldReportData: Bool) {
        addListeningSession(sessionListeningChannel)
        if shouldReportData {
            reportNextRadioData()
        }
    }

    /**
     Called when a radio event was received and it didn't start a new listening session, but we still want
     to sent the event data to TagStation.
     Note this will be a common occurrence. For instance, a radio event might have been received indicating
     that a new song was playing on the same session. This would not start/end a session, but we still want
     to report the radio event.

     - Parameter impressionRadioEvent: The radio event received.
     - Parameter shouldReportData: If true, the method implementation should also send the event data to
       TagStation.
     */
    @objc
    public func radioEventWasReceived(impressionRadioEvent: ImpressionRadioEvent, shouldReportData: Bool) {
        addImpressionRadioEvent(impressionRadioEvent)
        if shouldReportData {
            reportNextRadioData()
        }
    }

    // MARK: Location

    /**
     Starts the generation of updates that report the user’s current location.
     */
    @objc
    public func startUpdatingLocation() {
        DeviceLocation.sharedInstance.startUpdatingLocation()
    }

    /**
     Stops the generation of updates that report the user’s current location.
     */
    @objc
    public func stopUpdatingLocation() {
        DeviceLocation.sharedInstance.stopUpdatingLocation()
    }

    /**
     Requests the one-time delivery of the user’s current location.
     */
    @objc
    public func requestLocation() {
        DeviceLocation.sharedInstance.requestLocation()
    }

    /**
     Sets the location on the supplied NextRadio analtyics event data.
     This is done because TagStation prefers to have the data sent to it to have a latitude and
     longitude, but we don't want to make the host media app using this SDK to have to supply them.
     */
    private func setLocationOnNextRadioData(_ tagStationDataType: Locationable) {
        if let location = DeviceLocation.sharedInstance.location {
            tagStationDataType.setLocation(location: location)
        }
    }

    // MARK: Heartbeat

    /**
     Takes action when a scheduled NextRadio Reporting SDK "heartbeat" is triggered.
     This entails sending radio event impressions, listening sessions, and device location to TagStation.
     */
    @objc
    public func heartbeat() {
        os_log("---- NextRadioReportingSDK heartbeat", log: OSLog.default, type: .debug)

        let location = DeviceLocation.sharedInstance.makeLocation()
        if let location = location {
            eventData.addEventData(location)
        }

        if ListeningSession.sharedInstance.sessionIsActive {
            ListeningSession.sharedInstance.updateSesionLocation(location: location)
            let listeningSession = ListeningSession.sharedInstance.makeSession(sessionEnded: false)
            eventData.addEventData(listeningSession)
        }

        reportNextRadioData()
    }

    // MARK: Archive NextRadio Reporting objects

    /**
     Saves device registration data.
     */
    private func saveDeviceRegistrationData(deviceRegistrationData: DeviceRegistrationData) {
        do {
            os_log("Saving device registration data", log: OSLog.default, type: .debug)
            try deviceRegistrationData.saveObject()
        }
        catch {
            os_log("Unable to archive device registration data", log: OSLog.default, type: .error)
        }
    }

    /**
     Saves a NextRadio reporting key.
     */
    private func saveNextRadioReportingKey(nextRadioReportingKey: NextRadioReportingKey) {
        do {
            os_log("Saving NextRadio reporting key", log: OSLog.default, type: .debug)
            try nextRadioReportingKey.saveObject()
        }
        catch {
            os_log("Unable to archive NextRadio reporting key", log: OSLog.default, type: .error)
        }
    }

    /**
     Saves a TagStation device ID.
     */
    private func saveTagStationDeviceId(tagStationDeviceId: TagStationDeviceId) {
        do {
            os_log("Saving TagStation device ID", log: OSLog.default, type: .debug)
            try tagStationDeviceId.saveObject()
        }
        catch {
            os_log("Unable to archive TagStation device ID", log: OSLog.default, type: .error)
        }
    }
}
