//
//  Archives, unarchives, and deletes an object.
//
//  Created by John Koszarek on 11/29/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation

protocol Archivable {

    func saveObject() throws
    static func loadObject() -> Any?
    static func deleteObject() throws
}
