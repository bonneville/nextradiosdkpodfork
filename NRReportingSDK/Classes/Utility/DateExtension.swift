//
//  Extension to the Date class for sending date strings in the format TagStation perfers.
//
//  Created by John Koszarek on 12/20/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation

public extension Date {

    public func format() -> String! {
        let gmtTimeZone = TimeZone(abbreviation: "GMT")!
        let options: ISO8601DateFormatter.Options = [.withYear,
                                                     .withMonth,
                                                     .withDay,
                                                     .withTime,
                                                     .withDashSeparatorInDate,
                                                     .withColonSeparatorInTime]
        let formattedDate = ISO8601DateFormatter.string(from: self, timeZone: gmtTimeZone, formatOptions: options)
        return formattedDate
    }
}
