//
//  Created by John Koszarek on 10/24/17.
//  Copyright © 2017 NextRadio. All rights reserved.
//

import Foundation

///  A unique ID sesion to identify a related set of listening session data sent to TagStation's
///  analytics endpoint.
///  Currently using this as a transaction ID for data sent to TagStation in a single batch.
final class SessionId {

    var id: String

    init() {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        dateFormatter.dateFormat = "yyyyMMddHHmmssSSS"

        let currentTime = Date()
        id = dateFormatter.string(from: currentTime)
    }
}
