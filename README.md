## Synopsis

iOS SDK for NextRadio app reporting. This repository contains an Xcode project for building a dynamic Swift framework.

## Motivation

Source code repo built into an Xcode framework. This repository is not for development; use NextRadio Analytics SDK iOS Development for that.

